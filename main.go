package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")
	http.ServeFile(w, r, "./templates/index.html")
}

func main() {
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)

	router := mux.NewRouter()
	router.HandleFunc("/", handler).Methods("GET")
	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public")))).Methods("GET")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "https://ashutosh.celebal.com", http.StatusMovedPermanently)
	})

	log.Fatal(http.ListenAndServeTLS(":443", "/etc/letsencrypt/live/amoghpareek.in/fullchain.pem", "/etc/letsencrypt/live/amoghpareek.in/privkey.pem", nil))
	log.Println("ok")
}
